<?php

include_once __DIR__ . '/src/Autoloader.php';

Autoloader::setBaseDirectory(__DIR__);
Autoloader::addPrefix('TemplateEngine', 'src/TemplateEngine');
Autoloader::register();

if (\preg_match('/^(\/index.*|\/)$/', $_SERVER['REQUEST_URI'])) {
    include_once __DIR__ . '/src/form.php';
}
elseif (preg_match("/^\/form_handler.*$/", $_SERVER['REQUEST_URI'])) {
    include_once __DIR__ . '/src/form_handler.php';
}