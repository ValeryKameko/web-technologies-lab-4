<?php

function match_processor($match) {
    $day = $match['day'];
    $month = $match['month'];
    $year = $match['year'];
    if (strlen($year) == 2)
        $new_year = sprintf('%02d', ($year + 1) % 100);
    else
        $new_year = sprintf('%04d', ($year + 1) % 10000);
    return "<span class=\"date-text-element\">$day.$month.$new_year</span>";
}

function generate_date_regex() {
    $day_regex = '(?<day>0?[1-9]|[1-2][0-9]|3[0-1])';
    $month_regex = '(?<month>0?[1-9]|1[0-2])';
    $year_regex = '(?<year>\d{2,4})';

    $date_regex = '';

    $date_regex.= '/(?J)(';

    $date_regex.= $day_regex;
    $date_regex.= '\.';
    $date_regex.= $month_regex;
    $date_regex.= '\.';
    $date_regex.= $year_regex;

    $date_regex.= ')|(';

    $date_regex.= $month_regex;
    $date_regex.= '\/';
    $date_regex.= $day_regex;    
    $date_regex.= '\/';
    $date_regex.= $year_regex;
    
    $date_regex.= ')(?J)/';

    return $date_regex;
}

function process_text($raw_text) {
    return preg_replace_callback(generate_date_regex(), 'match_processor', $raw_text);
}