<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Форма</title>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="/css/form_style.css" rel="stylesheet">
    </head>
    <body>
        <div class="form-container">
            <form action="/form_handler.php" method="post" class="file-form" enctype="multipart/form-data">
                <div>
                    <h2 class="form-header">Обработать текст</h2>
                    <hr>
                </div>
                <div>
                    <label class="field-label">Файл:</label>
                    <input name="textfile" type="file" id="textfile" required>
                    <label class="field-input" for="textfile">
                        <div class="textfile-container">
                            <i class="fa fa-cloud-upload"></i> Загрузить файл
                        </div>
                    </label>
                </div>
                <input type="submit" value="Отфильтровать">
            </form>
        </div>
    </body>
</html>