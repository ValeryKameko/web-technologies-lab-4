<?php

namespace TemplateEngine;

use TemplateEngine\Node\Expression\Binary\SumBinaryExpressionNode;
use TemplateEngine\Node\Expression\Binary\MultBinaryExpressionNode;
use TemplateEngine\Node\Expression\Binary\NotEqualBinaryExpressionNode;
use TemplateEngine\Node\Expression\Binary\EqualBinaryExpressionNode;
use TemplateEngine\Node\Expression\Binary\LessBinaryExpressionNode;
use TemplateEngine\Node\Expression\Binary\GreaterBinaryExpressionNode;
use TemplateEngine\Node\Expression\Binary\LessOrEqualBinaryExpressionNode;
use TemplateEngine\Node\Expression\Binary\DivBinaryExpressionNode;
use TemplateEngine\Node\Expression\Binary\ModBinaryExpressionNode;
use TemplateEngine\Node\Expression\Binary\SubBinaryExpressionNode;
use TemplateEngine\Node\Expression\Binary\GreaterOrEqualBinaryExpressionNode;
use TemplateEngine\NodeParser\IfBlockNodeParser;
use TemplateEngine\NodeParser\ForBlockNodeParser;
use TemplateEngine\NodeParser\IncludeBlockNodeParser;
use TemplateEngine\NodeParser\RawIncludeBlockNodeParser;
use TemplateEngine\NodeParser\ConfigIncludeBlockNodeParser;
use TemplateEngine\NodeParser\DBIncludeBlockNodeParser;
use TemplateEngine\Template;

class Environment
{
    private $options;
    private $loader;
    private $resourseLoader;

    public function __construct($loader, $options = []) 
    {
        $this->options = array_merge([
            'template_lexer' => [
                'block_brackets' => ['{%', '%}'],
                'expression_brackets' => ['{{', '}}'],
                'comment_brackets' => ['{#', '#}'],
                'whitespace_trim' => '-',
                'number_regex' => '([1-9]\d+|\d)(\.\d+)?',
                'name_regex' => '[_a-zA-Z\x7f-\xff][_a-zA-Z0-9\x7f-\xff]*',
                'unary_operators' => ['-', '+', '!'],
                'binary_operators' => ['&&', '||', '-', '+', '*', '/', '%', '.', '=', '!=','==', '<=', '>=', '>', '<'],
                'punctuation' => [',', '(', ')', '[', ']', '{', '}', '?', ':', '=>'],
                'whitespaces' => ['\s'],
                'raw_string_quote' => '\'',
            ],
            'block_parsers' => [
                'if' => IfBlockNodeParser::class,
                'for' => ForBlockNodeParser::class,
                'include' => IncludeBlockNodeParser::class,
                'raw_include' => RawIncludeBlockNodeParser::class,
                'config_include' => ConfigIncludeBlockNodeParser :: class,
                'db_include' => DBIncludeBlockNodeParser :: class
            ],
            'functions' => [
                'range' => 'range',
                'is_array' => 'is_array',
                'is_number' => 'is_numeric',
                'is_bool' => 'is_bool',
                'is_string' => 'is_string',
                'is_iterable' => 'is_iterable'
            ],
            'expression_parser' => ExpressionParser::class,
            'lexer' => TemplateLexer::class,
            'compiler' => Compiler::class,
            'parser' => Parser::class,
            'special_names' => [
                '__parent',
                '__seq',
                '__key',
                '__value',
                '__functions',
                '__config_include_dir',
                'db_provider',
                '__raw_include_dir'
            ],
            'classes_cnt' => 0,
            'special_names_escape_suffix' => '_escaped',
            'unary_operators' => [
            ],
            'binary_operators' => [
                '!=' => [
                    'precedence' => 7,
                    'associativity' => ExpressionParser::ASSOCITIVITY_LEFT,
                    'class' => NotEqualBinaryExpressionNode::class
                ],
                '==' => [
                    'precedence' => 7,
                    'associativity' => ExpressionParser::ASSOCITIVITY_LEFT,
                    'class' => EqualBinaryExpressionNode::class
                ],
                '<' => [
                    'precedence' => 7,
                    'associativity' => ExpressionParser::ASSOCITIVITY_LEFT,
                    'class' => LessBinaryExpressionNode::class
                ],
                '<=' => [
                    'precedence' => 7,
                    'associativity' => ExpressionParser::ASSOCITIVITY_LEFT,
                    'class' => LessOrEqualBinaryExpressionNode::class
                ],
                '>' => [
                    'precedence' => 7,
                    'associativity' => ExpressionParser::ASSOCITIVITY_LEFT,
                    'class' => GreaterBinaryExpressionNode::class
                ],
                '>=' => [
                    'precedence' => 7,
                    'associativity' => ExpressionParser::ASSOCITIVITY_LEFT,
                    'class' => GreaterOrEqualBinaryExpressionNode::class
                ],
                '+' => [
                    'precedence' => 9,
                    'associativity' => ExpressionParser::ASSOCITIVITY_LEFT,
                    'class' => SumBinaryExpressionNode::class
                ],
                '-' => [
                    'precedence' => 9,
                    'associativity' => ExpressionParser::ASSOCITIVITY_LEFT,
                    'class' => SubBinaryExpressionNode::class
                ],
                '~' => [
                    'precedence' => 9,
                    'associativity' => ExpressionParser::ASSOCITIVITY_LEFT,
                    'class' => ConcatBinaryExpressionNode::class
                ],
                '*' => [
                    'precedence' => 10,
                    'associativity' => ExpressionParser::ASSOCITIVITY_LEFT,
                    'class' => MultBinaryExpressionNode::class
                ],
                '%' => [
                    'precedence' => 10,
                    'associativity' => ExpressionParser::ASSOCITIVITY_LEFT,
                    'class' => ModBinaryExpressionNode::class
                ],
                '/' => [
                    'precedence' => 10,
                    'associativity' => ExpressionParser::ASSOCITIVITY_LEFT,
                    'class' => DivBinaryExpressionNode::class
                ]
            ],
        ], $options);
        $this->loader = $loader;
    }

    public function setOption($keys, $value)
    {
        if (is_string($keys))
            $this->options[$keys] = $value;
        else {
            $currentOption = &$this->options;
            foreach ($keys as $key) {
                $currentOption = &$currentOption[$key];
            }
            $currentOption = $value;
        }
    }

    public function loadConfigFile($filePath) {
        $configContent = \parse_ini_file($filePath);
        return $configContent;
    }

    public function displayRawFile($filePath) {
        echo file_get_contents($filePath);
    }

    public function load($templatePath) {
        $source = $this->loader->load($templatePath);
        $className = $this->generateClassName($source);
        $sourceCode = $this->compileSource($source, $className);

        eval($sourceCode);

        return new $className($this, $source);
    }

    public function compile($templatePath) {
        $source = $this->loader->load($templatePath);
        $className = $this->generateClassName($source);
        $sourceCode = $this->compileSource($source, $className);

        return $sourceCode;
    }

    private function compileSource($source, $className) {
        $lexerClass = $this->getOption('lexer');
        $lexer = new $lexerClass($this, []);
        $stream = $lexer->tokenize($source);

        $parserClass = $this->getOption('parser');
        $parser = new $parserClass($this);
        $templateNode = $parser->parse($stream);

        $compilerClass = $this->getOption('compiler');
        $compiler = new $compilerClass($this);
        $classSource = $this->compileClass($compiler, $className, $source, $templateNode);

        return $classSource;
    }

    private function compileClass($compiler, $className, $source, $templateNode) {
        $this->compileClassHeader($compiler, $className);
        $this->compileClassConstructor($compiler);
        $this->compileDisplayFunction($compiler, $templateNode);

        $compiler->outdent();
        $compiler->writeLine('}');
        return $compiler->getSourceCode();
    }

    private function compileClassConstructor($compiler) {
        $compiler->writeLine('public function __construct($env, $source)');
        $compiler->writeLine('{');
        $compiler->indent();

        $compiler->writeLine('parent::__construct($env, $source);');
        $compiler->outdent();
        $compiler->writeLine('}');
        $compiler->writeLine('');
    }

    private function compileClassHeader($compiler, $className) {
        $compiler->write('use ' . Template::class . ';');
        $compiler->endLine();
        $compiler->endLine();

        $compiler->writeLine("class $className extends Template");
        $compiler->writeLine('{');
        $compiler->indent();
    }

    private function compileDisplayFunction($compiler, $templateNode) {
        $compiler->writeLine('protected function displaySelf($context)');
        $compiler->writeLine('{');
        $compiler->indent();

        $compiler->subcompile($templateNode);

        $compiler->outdent();
        $compiler->writeLine('}');
    }

    public function setOptions($options)
    {
        $this->options = array_merge($options);
    }

    private function generateClassName($source) {
        return '__template_num_' . (++$this->options['classes_cnt']);
    }

    public function getOption($keys)
    {
        if (is_string($keys))
            return $this->options[$keys];
        else {
            $currentOption = &$this->options;
            foreach ($keys as $key) {
                $currentOption = &$currentOption[$key];
            }
            return $currentOption;
        }
    }
}