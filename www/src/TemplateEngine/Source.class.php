<?php

namespace TemplateEngine;

class Source
{
    private $name;
    private $code;
    private $fullPath;

    public function __construct($name, $code, $fullPath = NULL)
    {
        $this->name = $name;
        $this->code = $code;
        $this->fullPath = $fullPath;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getFullPath()
    {
        return $this->fullPath;
    }

    public function hasFullPath()
    {
        return is_null($this->fullPath);
    }
}