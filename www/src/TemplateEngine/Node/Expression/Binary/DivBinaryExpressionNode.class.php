<?php

namespace TemplateEngine\Node\Expression\Binary;

use TemplateEngine\Compiler;

class DivBinaryExpressionNode extends AbstractBinaryExpressionNode
{
    public function compileOperator(Compiler $compiler)
    {
        $compiler->write('/');
    }
}