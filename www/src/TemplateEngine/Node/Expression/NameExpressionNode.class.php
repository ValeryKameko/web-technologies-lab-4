<?php

namespace TemplateEngine\Node\Expression;

use TemplateEngine\Error\UnimlementedError;
use TemplateEngine\Compiler;

class NameExpressionNode extends AbstractExpressionNode
{
    
    public function __construct($name, $line)
    {
        parent::__construct([], [ 'name' => $name ], $line, 'name');
    }

    public function compile(Compiler $compiler)
    {
        $escapedName = $this->escapedName($compiler);
        $compiledVariableReference = 
            "((isset(\$context['$escapedName']) || array_key_exists('$escapedName', \$context)) ? \$context['$escapedName'] : null)";

        $compiler->write($compiledVariableReference);
    }

    public function compileNoCheck(Compiler $compiler) {
        $escapedName = $this->escapedName($compiler);
        $compiledVariableReference = 
            "\$context['$escapedName']";
        $compiler->write($compiledVariableReference);
    }

    private function escapedName(Compiler $compiler) {
        if ($this->needEscape($compiler, $this->attributes['name'])) 
            $escapedName = $this->attributes['name'] . $compiler->getOption('special_names_escape_suffix');
        else
            $escapedName = $this->attributes['name'];
        return $escapedName;
    }

    private function needEscape($compiler, $name)
    {
        $specialNames = $compiler->getEnv()->getOption('special_names');
        $isSpecialName = false;
        foreach ($specialNames as $specialName) {
            if (\substr($name, 0, \strlen($specialName)) === $specialName) {
                $isSpecialName = true;
                break;
            }
        }
        return $isSpecialName;
    }
}