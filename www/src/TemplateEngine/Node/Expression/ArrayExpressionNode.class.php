<?php

namespace TemplateEngine\Node\Expression;

use TemplateEngine\Error\UnimlementedError;
use TemplateEngine\Compiler;

class ArrayExpressionNode extends AbstractExpressionNode
{
    public function __construct($elements, $line)
    {
        parent::__construct([ 'elements' => $elements ], [ ], $line, 'array');
    }

    public function compile(Compiler $compiler)
    {
        $compiler->write('[ ');
        $isFirstElement = true;
        foreach ($this->getNode('elements') as $element) {
            if ($isFirstElement)
                $isFirstElement = false;
            else 
                $compiler->write(', ');
            $element->compile($compiler);
        }
        $compiler->write(' ]');
    }
}
