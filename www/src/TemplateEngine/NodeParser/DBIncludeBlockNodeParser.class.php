<?php

namespace TemplateEngine\NodeParser;

use TemplateEngine\Parser;
use TemplateEngine\Token;
use TemplateEngine\Node\DBIncludeBlockNode;

class DBIncludeBlockNodeParser extends AbstractNodeParser
{
    public function __construct()
    {

    }

    public function subparse(Parser $parser)
    {
        $line = $parser->getLine();
        $parser->expect(Token::NAME_TYPE, 'db_include');
        $dbIncludeFilePathExpression = $parser->parseExpression();
        return new DBIncludeBlockNode($dbIncludeFilePathExpression, $line);
    }
}