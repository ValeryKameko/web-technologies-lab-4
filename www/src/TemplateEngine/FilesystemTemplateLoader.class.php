<?php

namespace TemplateEngine;

use TemplateEngine\Source;

class FilesystemTemplateLoader {
    private $directoryPath;

    public function __construct($directoryPath) {
        $this->directoryPath = rtrim($directoryPath, DIRECTORY_SEPARATOR);
    }

    public function load($templatePath) {
        $templatePath = ltrim($templatePath, DIRECTORY_SEPARATOR);
        $fullPath = $this->getFullPath($templatePath);
        $code = $this->loadTemplate($fullPath);
        $source = new Source(basename($templatePath), $code, $fullPath);
        return $source;
    }

    private function loadTemplate($fullPath) {
        return \file_get_contents($fullPath);
    }

    private function getFullPath($relativePath) {
        return $this->directoryPath . DIRECTORY_SEPARATOR . $relativePath;
    }
}