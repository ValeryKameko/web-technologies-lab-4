<?php

use TemplateEngine\Environment;
use TemplateEngine\FilesystemTemplateLoader;

$env = new Environment(new FilesystemTemplateLoader(dirname(__DIR__) . '/template'));

$template = $env->load('form_handler_view.tmpl');
$template->render([
    'raw_text' => $raw_text,
    'processed_text' => $processed_text,
]);