<?php

include_once __DIR__ . '/processing_text.php';

if (!isset($_FILES['textfile'])) {
    header('Location: /index.php');
    die();
}

$data = file_get_contents($_FILES['textfile']['tmp_name']);

// var_dump($data);

$raw_text = htmlspecialchars($data);

$processed_text = process_text($raw_text);

include_once __DIR__ . '/form_handler_view.php';
